use staging;

INSERT INTO product_client_a (name, description, artnumber, price, stock, deleted, brand_name)
VALUES
('Product A1', 'Description A1', 'ART001', 100, 50, false, 'Brand A'),
('Product A2', 'Description A2', 'ART002', 150, 30, false, 'Brand B'),
('Product A3', 'Description A3', 'ART003', 120, 40, false, 'Brand C'),
('Product A4', 'Description A4', 'ART004', 80, 20, false, 'Brand A'),
('Product A5', 'Description A5', 'ART005', 200, 10, false, 'Brand B'),
('Product A6', 'Description A6', 'ART006', 90, 15, false, 'Brand C'),
('Product A7', 'Description A7', 'ART007', 110, 25, false, 'Brand A'),
('Product A8', 'Description A8', 'ART008', 130, 35, false, 'Brand B'),
('Product A9', 'Description A9', 'ART009', 70, 45, false, 'Brand C'),
('Product A10', 'Description A10', 'ART010', 180, 5, false, 'Brand A');
INSERT INTO product_client_b (nama, deskripsi, nomor_artikel, harga, persediaan_barang, dihapus, nama_merek)
VALUES
('Produk B11', 'Deskripsi B11', 'ARRT011', 120, 25, false, 'Merek B'),
('Produk B12', 'Deskripsi B12', 'ARRT012', 80, 15, false, 'Merek C'),
('Produk B13', 'Deskripsi B13', 'ARRT013', 150, 35, false, 'Merek A'),
('Produk B14', 'Deskripsi B14', 'ARRT014', 100, 20, false, 'Merek B'),
('Produk B15', 'Deskripsi B15', 'ARRT015', 130, 30, false, 'Merek C'),
('Produk B16', 'Deskripsi B16', 'ARRT016', 90, 10, false, 'Merek A'),
('Produk B17', 'Deskripsi B17', 'ARRT017', 110, 40, false, 'Merek B'),
('Produk B18', 'Deskripsi B18', 'ARRT018', 180, 5, false, 'Merek C'),
('Produk B19', 'Deskripsi B19', 'ARRT019', 200, 45, false, 'Merek A'),
('Produk B20', 'Deskripsi B20', 'ARRT020', 70, 30, false, 'Merek B');
