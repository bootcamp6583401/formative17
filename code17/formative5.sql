CREATE TABLE brand (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

INSERT INTO brand (name) VALUES ('Brand1');
INSERT INTO brand (name) VALUES ('Brand2');
INSERT INTO brand (name) VALUES ('Brand3');
INSERT INTO brand (name) VALUES ('Brand4');
INSERT INTO brand (name) VALUES ('Brand5');

UPDATE product SET brand_id = 1;



ALTER TABLE product
DROP COLUMN brand;


ALTER TABLE product
ADD COLUMN brand_id INT NOT NULL;

ALTER TABLE product
ADD FOREIGN KEY (brand_id) REFERENCES brand(id);

-- SELECT
--     CONSTRAINT_NAME,
--     TABLE_NAME,
--     CONSTRAINT_TYPE
--     FROM
--     INFORMATION_SCHEMA.TABLE_CONSTRAINTS
--     WHERE
--     TABLE_SCHEMA = 'production';