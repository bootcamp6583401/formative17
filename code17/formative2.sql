  use staging;
  
  CREATE TABLE product_client_a (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255),
    description TEXT,
    artnumber VARCHAR(50),
    price INT,
    stock INT,
    deleted BOOLEAN,
    brand_name VARCHAR(255)
);

CREATE TABLE product_client_b (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nama VARCHAR(255),
    deskripsi TEXT,
    nomor_artikel VARCHAR(50),
    harga INT,
    persediaan_barang INT,
    dihapus BOOLEAN,
    nama_merek VARCHAR(255)
);


use production;

CREATE TABLE product (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255),
    description TEXT,
    art_number VARCHAR(50),
    price INT,
    stock INT,
    is_deleted BOOLEAN,
    brand VARCHAR(255)
);