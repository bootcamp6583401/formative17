package formative.seventeen.api;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import formative.seventeen.api.models.Brand;
import formative.seventeen.api.models.Product;
import formative.seventeen.api.repositories.ProductRepository;
import formative.seventeen.api.services.impl.ProductService;

@ExtendWith(MockitoExtension.class)
public class ProductDataTest {
    @Mock
    private ProductRepository repo;

    @InjectMocks
    private ProductService service;

    private Product product;

    private Brand brand;

    @BeforeEach
    public void setup() {
        product = new Product();
        product.setId(333);
        product.setArtNumber("Art12");
        product.setDeleted(false);
        product.setDescription("sewag");
        product.setPrice(333);
        product.setStock(23);

        brand = new Brand();

        brand.setId(1);
        brand.setName("Swag");
    }

    @Test
    public void saveandreturn() {
        Product product = new Product();
        product.setId(23333);
        product.setArtNumber("Art12");
        product.setDeleted(false);
        product.setDescription("sewag");
        product.setPrice(333);
        product.setStock(23);

        product.setBrand(brand);

        when(repo.save(ArgumentMatchers.any(Product.class))).thenReturn(product);
        Product created = service.save(product);
        assertEquals(product.getArtNumber(), created.getArtNumber());
        assertEquals(product.getDescription(), created.getDescription());
        assertEquals(product.getId(), created.getId());
        assertEquals(product.getStock(), created.getStock());
        assertEquals(product.getPrice(), created.getPrice());
        assertEquals(product.isDeleted(), created.isDeleted());
        assertTrue(product.equals(created));
        assertEquals(product.hashCode(), created.hashCode());
        verify(repo).save(product);
    }

    @Test
    public void updateNull() {
        Product newProd = new Product();
        newProd.setArtNumber("New Test Name");
        newProd.setBrand(brand);
        when(repo.findById(3)).thenReturn(Optional.ofNullable(null));

        assertNull(service.updateById(3, newProd));
    }

    // JUnit test for getAllProducts method
    @DisplayName("JUnit test for getAllProducts method")
    @Test
    public void givenProductsList_whenGetAllProducts_thenReturnProductsList() {
        // given - precondition or setup

        Product product1 = new Product();
        Product product2 = new Product();

        when(repo.findAll()).thenReturn(List.of(product1, product2));

        // when - action or the behaviour that we are going test
        List<Product> products = StreamSupport.stream(service.getAll().spliterator(), false)
                .collect(Collectors.toList());
        ;

        // then - verify the output
        assertNotNull(products);
        assertEquals(2, products.size());
    }

    // JUnit test for getProductById method
    @DisplayName("JUnit test for getProductById method")
    @Test
    public void givenProductId_whenGetProductById_thenReturnProductObject() {

        Product product = new Product();
        product.setId(89);
        when(repo.findById(product.getId())).thenReturn(Optional.of(product));
        Product expected = service.getById(product.getId());
        assertEquals(expected, product);
        verify(repo).findById(product.getId());

    }

    @Test
    public void whenGivenId_shouldDeleteProduct_ifFound() {
        Product product = new Product();
        product.setArtNumber("Test Name");
        product.setId(3444);
        service.deleteById(product.getId());
        verify(repo).deleteById(product.getId());
        verify(repo, times(1)).deleteById(product.getId());

    }

    @Test
    public void whenGivenId_shouldUpdateProduct_ifFound() {
        Product product = new Product();
        product.setId(893);
        product.setArtNumber("Test Name");
        Product newProduct = new Product();
        product.setArtNumber("New Test Name");
        when(repo.findById(product.getId())).thenReturn(Optional.of(product));
        service.updateById(product.getId(), newProduct);
        verify(repo).save(newProduct);
        verify(repo).findById(product.getId());
    }

    // // JUnit test for updateProduct method
    // @DisplayName("JUnit test for updateProduct method")
    // @Test
    // public void givenProductObject_whenUpdateProduct_thenReturnUpdatedProduct(){
    // // given - precondition or setup
    // given(repo.save(employee)).willReturn(employee);
    // employee.setEmail("ram@gmail.com");
    // employee.setFirstName("Ram");
    // // when - action or the behaviour that we are going test
    // Product updatedProduct = employeeService.updateProduct(employee);

    // // then - verify the output
    // assertThat(updatedProduct.getEmail()).isEqualTo("ram@gmail.com");
    // assertThat(updatedProduct.getFirstName()).isEqualTo("Ram");
    // }

    // // JUnit test for deleteProduct method
    // @DisplayName("JUnit test for deleteProduct method")
    // @Test
    // public void givenProductId_whenDeleteProduct_thenNothing(){
    // // given - precondition or setup
    // long employeeId = 1L;

    // willDoNothing().given(repo).deleteById(employeeId);

    // // when - action or the behaviour that we are going test
    // employeeService.deleteProduct(employeeId);

    // // then - verify the output
    // verify(repo, times(1)).deleteById(employeeId);
    // }
}
