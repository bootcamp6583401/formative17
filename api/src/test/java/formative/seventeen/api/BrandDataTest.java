package formative.seventeen.api;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import formative.seventeen.api.models.Brand;
import formative.seventeen.api.repositories.BrandRepository;
import formative.seventeen.api.services.impl.BrandService;

@ExtendWith(MockitoExtension.class)
public class BrandDataTest {
    @Mock
    private BrandRepository repo;

    @InjectMocks
    private BrandService service;

    private Brand brand;

    @BeforeEach
    public void setup() {
        brand = new Brand();
        brand.setId(333);
        brand.setName("Brand Name");
    }

    @Test
    public void saveandreturn() {
        Brand brand = new Brand();
        brand.setName("Test Name");
        when(repo.save(ArgumentMatchers.any(Brand.class))).thenReturn(brand);
        Brand created = service.save(brand);
        assertEquals(brand.getName(), created.getName());
        assertTrue(brand.equals(created));
        assertEquals(brand.hashCode(), created.hashCode());
        verify(repo).save(brand);
    }

    // JUnit test for getAllBrands method
    @DisplayName("JUnit test for getAllBrands method")
    @Test
    public void givenBrandsList_whenGetAllBrands_thenReturnBrandsList() {
        // given - precondition or setup

        Brand brand1 = new Brand();
        Brand brand2 = new Brand();

        when(repo.findAll()).thenReturn(List.of(brand1, brand2));

        // when - action or the behaviour that we are going test
        List<Brand> brands = StreamSupport.stream(service.getAll().spliterator(), false)
                .collect(Collectors.toList());
        ;

        // then - verify the output
        assertNotNull(brands);
        assertEquals(2, brands.size());
    }

    // JUnit test for getBrandById method
    @DisplayName("JUnit test for getBrandById method")
    @Test
    public void givenBrandId_whenGetBrandById_thenReturnBrandObject() {

        Brand brand = new Brand();
        brand.setId(89);
        when(repo.findById(brand.getId())).thenReturn(Optional.of(brand));
        Brand expected = service.getById(brand.getId());
        assertEquals(expected, brand);
        verify(repo).findById(brand.getId());

    }

    @Test
    public void whenGivenId_shouldDeleteBrand_ifFound() {
        Brand brand = new Brand();
        brand.setName("Test Name");
        brand.setId(3444);
        service.deleteById(brand.getId());
        verify(repo).deleteById(brand.getId());
        verify(repo, times(1)).deleteById(brand.getId());

    }

    @Test
    public void whenGivenId_shouldUpdateBrand_ifFound() {
        Brand brand = new Brand();
        brand.setId(893);
        brand.setName("Test Name");
        Brand newBrand = new Brand();
        brand.setName("New Test Name");
        when(repo.findById(brand.getId())).thenReturn(Optional.of(brand));
        service.updateById(brand.getId(), newBrand);
        verify(repo).save(newBrand);
        verify(repo).findById(brand.getId());
    }

    @Test
    public void updateNull() {
        Brand newBrand = new Brand();
        newBrand.setName("New Test Name");
        when(repo.findById(3)).thenReturn(Optional.ofNullable(null));

        assertNull(service.updateById(3, newBrand));
    }

    // // JUnit test for updateBrand method
    // @DisplayName("JUnit test for updateBrand method")
    // @Test
    // public void givenBrandObject_whenUpdateBrand_thenReturnUpdatedBrand(){
    // // given - precondition or setup
    // given(repo.save(employee)).willReturn(employee);
    // employee.setEmail("ram@gmail.com");
    // employee.setFirstName("Ram");
    // // when - action or the behaviour that we are going test
    // Brand updatedBrand = employeeService.updateBrand(employee);

    // // then - verify the output
    // assertThat(updatedBrand.getEmail()).isEqualTo("ram@gmail.com");
    // assertThat(updatedBrand.getFirstName()).isEqualTo("Ram");
    // }

    // // JUnit test for deleteBrand method
    // @DisplayName("JUnit test for deleteBrand method")
    // @Test
    // public void givenBrandId_whenDeleteBrand_thenNothing(){
    // // given - precondition or setup
    // long employeeId = 1L;

    // willDoNothing().given(repo).deleteById(employeeId);

    // // when - action or the behaviour that we are going test
    // employeeService.deleteBrand(employeeId);

    // // then - verify the output
    // verify(repo, times(1)).deleteById(employeeId);
    // }
}
