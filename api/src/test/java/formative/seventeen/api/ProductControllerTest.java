package formative.seventeen.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import formative.seventeen.api.models.Product;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class ProductControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void setup() throws Exception {
        this.mockMvc.perform(post("/products").contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\": 333, \"artNumber\": \"test\", \"brand\": {\"id\": 1}}")
                .accept(MediaType.APPLICATION_JSON));
    }

    @Test
    void getAll() throws Exception {
        this.mockMvc.perform(get("/products")).andDo(print()).andExpect(status().isOk())
                .andExpect(status().isOk());
    }

    @Test
    void getById() throws Exception {
        this.mockMvc.perform(get("/products/5")).andDo(print()).andExpect(status().isOk())
                .andExpect(status().isOk());
    }

    @Test
    void saveDate() throws Exception {
        this.mockMvc.perform(post("/products").contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\": 333, \"artNumber\": \"test\", \"brand\": {\"id\": 1}}")

                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @Test
    void deleteById() throws Exception {
        this.mockMvc.perform(delete("/products/333333")).andDo(print()).andExpect(status().isOk())
                .andExpect(status().isOk());
    }

    @Test
    void updateer() throws Exception {
        this.mockMvc.perform(put("/products/6").contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\": 333, \"artNumber\": \"test\", \"brand\": {\"id\": 1}}")
                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

}