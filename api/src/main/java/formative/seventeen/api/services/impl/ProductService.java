package formative.seventeen.api.services.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import formative.seventeen.api.models.Product;
import formative.seventeen.api.repositories.ProductRepository;
import jakarta.persistence.EntityNotFoundException;

@Service
public class ProductService {

    @Autowired
    ProductRepository repo;

    public Iterable<Product> getAll() {
        return repo.findAll();
    }

    public Product save(Product address) {
        return repo.save(address);
    }

    public Product getById(int id) {
        return repo.findById(id).orElseThrow(() -> new EntityNotFoundException("Product not found"));
    }

    public Product updateById(int id, Product address) {
        Optional<Product> res = repo.findById(id);

        if (res.isPresent()) {
            address.setId(id);
            return repo.save(address);
        }

        return null;
    }

    public void deleteById(int id) {
        repo.deleteById(id);
    }

}