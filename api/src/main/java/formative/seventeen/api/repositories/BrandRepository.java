package formative.seventeen.api.repositories;

import org.springframework.data.repository.CrudRepository;

import formative.seventeen.api.models.Brand;

public interface BrandRepository extends CrudRepository<Brand, Integer> {
}
