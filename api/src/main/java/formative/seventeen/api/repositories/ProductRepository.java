package formative.seventeen.api.repositories;

import org.springframework.data.repository.CrudRepository;

import formative.seventeen.api.models.Product;

public interface ProductRepository extends CrudRepository<Product, Integer> {
}
